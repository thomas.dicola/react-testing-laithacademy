import Counter from '../Counter';
import { render, fireEvent, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

let getByTestId;

beforeEach(() => {
  const component = render(<Counter />);
  getByTestId = component.getByTestId;
});

test('header renders with correct text', () => {
  const headerElement = getByTestId('header');

  expect(headerElement.textContent).toBe('My Counter');
});

test('counter initally start with text of 0', () => {
  const counterElement = getByTestId('counter');

  expect(counterElement.textContent).toBe('0');
});

test('input contains initial value of 1', () => {
  const inputElement = getByTestId('input');

  expect(inputElement.value).toBe('1');
});

test('add button renders with +', () => {
  const addBtn = getByTestId('add-btn');

  expect(addBtn.textContent).toBe('+');
});

test('add button renders with -', () => {
  const subtractBtn = getByTestId('subtract-btn');

  expect(subtractBtn.textContent).toBe('-');
});

test('changing value of input works correctly', () => {
  const inputElement = getByTestId('input');

  fireEvent.change(inputElement, {
    target: {
      value: '5',
    },
  });

  expect(inputElement.value).toBe('5');
});

test('click on plus btn adds 1 to counter', () => {
  const addBtnElement = getByTestId('add-btn');
  const counterElement = getByTestId('counter');
  expect(counterElement.textContent).toBe('0');

  fireEvent.click(addBtnElement);

  expect(counterElement.textContent).toBe('1');
});

test('click on subtract btn subtracts 1 to counter', () => {
  const subtractBtnElement = getByTestId('subtract-btn');
  const counterElement = getByTestId('counter');
  expect(counterElement.textContent).toBe('0');

  fireEvent.click(subtractBtnElement);

  expect(counterElement.textContent).toBe('-1');
});

test('changing input value then clicking on add btn works correctyl', () => {
  const addBtnElement = getByTestId('add-btn');
  const counterElement = getByTestId('counter');
  const inputElement = getByTestId('input');

  fireEvent.change(inputElement, {
    target: {
      value: '5',
    },
  });

  fireEvent.click(addBtnElement);

  expect(counterElement.textContent).toBe('5');
});

test('changing input value then clicking on add btn works correctyl', () => {
  const subtractBtnElement = getByTestId('subtract-btn');
  const counterElement = getByTestId('counter');
  const inputElement = getByTestId('input');

  fireEvent.change(inputElement, {
    target: {
      value: '5',
    },
  });

  fireEvent.click(subtractBtnElement);

  expect(counterElement.textContent).toBe('-5');
});

test('adding and then substracting leads to the correct counter number', () => {
  const subtractBtnElement = getByTestId('subtract-btn');
  const addBtnElement = getByTestId('add-btn');
  const counterElement = getByTestId('counter');
  const inputElement = getByTestId('input');

  fireEvent.change(inputElement, {
    target: {
      value: '10',
    },
  });

  fireEvent.click(addBtnElement);
  fireEvent.click(addBtnElement);
  fireEvent.click(addBtnElement);
  fireEvent.click(addBtnElement);
  fireEvent.click(subtractBtnElement);
  fireEvent.click(subtractBtnElement);

  expect(counterElement.textContent).toBe('20');

  fireEvent.change(inputElement, {
    target: {
      value: '5',
    },
  });

  fireEvent.click(addBtnElement);
  fireEvent.click(subtractBtnElement);
  fireEvent.click(subtractBtnElement);

  expect(counterElement.textContent).toBe('15');
});

test('counter contains correct className', () => {
  const counterEl = getByTestId('counter');
  const inputEl = getByTestId('input');
  const addBtnEl = getByTestId('add-btn');
  const subtractBtnEl = getByTestId('subtract-btn');

  expect(counterEl.className).toBe('');

  fireEvent.change(inputEl, {
    target: {
      value: '50',
    },
  });

  fireEvent.click(addBtnEl);

  expect(counterEl.className).toBe('');

  fireEvent.click(addBtnEl);

  expect(counterEl.className).toBe('green');

  fireEvent.click(subtractBtnEl);

  expect(counterEl.className).toBe('');

  fireEvent.click(subtractBtnEl);
  fireEvent.click(subtractBtnEl);
  fireEvent.click(subtractBtnEl);

  expect(counterEl.className).toBe('red');
});
